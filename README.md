Wiki Loves Geo
==============

* Hack day project I built at History Hack Day in January 2011.
* Uses Ruby and MongoDB
* Grossly and horribly unfinished

Functional description
----------------------

The functions in module.rb are there to handle communication between Wikipedia and MongoDB.

The functions scrape stuff from [Category:Wikipedia requested photographs in places](https://en.wikipedia.org/wiki/Category:Wikipedia_requested_photographs_in_places) and put them in to a MongoDB collection with the lat-long extracted from the microformat on the page.

You can then query MongoDB to retrieve stuff in the normal way.

An example of this is serve.rb: this is a little Twitter user-stream based server which follows people on Twitter and if they post a geotagged message, sends back a tweet with the nearest thing to them.

Coverage
--------

* [YDN blog post](http://developer.yahoo.com/blogs/ydn/posts/2011/01/history-hack-day-it-was-therefore-it-now-isnt/)