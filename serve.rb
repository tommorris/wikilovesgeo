require 'rubygems'
require 'user-stream-receiver'
require 'json'
require 'mongo'
require 'bson'
require 'active_support'
require 'twitter'
#require 'bitly'
require 'oauth'

$oauth = $settings[:twitter_oauth]

# set up our bitly shortener                                                                                                                                
#Bitly.use_api_version_3
#$shortener = Bitly.new($settings[:bitly][:username], $settings[:bitly][:key])

# 3786464
$twitter = Twitter::Client.new(options = $oauth)
$mongo = Mongo::Connection.new.db($settings[:mongo][:collection_name])

# original js: db.runCommand({geoNear: "locations", near : [30.262528, -97.740119], maxDistance: 0.05, num: 8})
# lookup(30.262528, -97.740119)
def lookup(lat, long)
  cmd = BSON::OrderedHash.new
  cmd['geoNear'] = "locations"
  cmd['near'] = [lat, long]
  cmd['num'] = 1
  cmd['maxDistance'] = 0.1
  $mongo.command(cmd)
end

UserStreamReceiver.new.run {|c|
  puts c
  chunk = JSON.parse(c) unless c.strip = ''
  if chunk["geo"] && chunk["geo"]["type"] == "Point"
    data = {
      :user => chunk["user"]["screen_name"],
      :location => chunk["geo"]["coordinates"],
      :created_at => chunk["user"]["created_at"]
    }
    resp = lookup(data[:location][0], data[:location][1])
    if resp["results"].size != 0
      $twitter.direct_message_create(data[:user], "Could you help illustrate '#{resp["results"][0]["obj"]["name"]}' on Wikipedia?")
      # #{$shortener.shorten(i[:link])}")
    end
    # eventually, we need to check and see what the last message was we sent.
  end
}