require 'mongo'
require 'curb'
require 'nokogiri'
require 'rdf'
require 'linkeddata'
require 'mofo'

def get(url)
  x = Curl::Easy.new do |c|
    c.headers["User-Agent"] = "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.4) Gecko/20100513 Firefox/3.6.4"
  end
  x.url = url
  x.perform
  return x.body_str
end

def scrape(url)
  doc = Nokogiri::HTML(get(url))
  pages = doc.search("#mw-pages li a").map {|i| i.attributes["href"].value.split(":").last.gsub(/_/, " ") }
  
  pagers = doc.search("a").select{|x| x.text == "next 200" }.map {|i| i.attributes["href"].text }.uniq
  
  subcats = doc.search("#mw-subcategories li a").map {|i| i.attributes["href"].text }
  
  pages.each {|stub|
    $queue << [:page, stub]
  }
  subcats.each {|stub|
    $queue << [:category, stub]
  }
  pagers.each {|stub|
    $queue << [:category, stub]
  }
end

def procQueueJob(arr)
  if arr.first == :page
    data = getCoordsFromRdf(arr.last.gsub(/ /, "_"))
    if data != nil
      mongoStore(arr.last, data)
    end
  end
  
  if arr.first == :category
    puts "Started parsing category: " + arr.last + " -- qsize: " + $queue.size.to_s
    scrape("http://en.wikipedia.org" + arr.last)
  end
end

def mongoStore(stub, coords)
  $locations ||= Mongo::Connection.new.db("plusfour").collection("locations")
  doc = {
    "name" => stub.gsub(/_/, " "),
    "loc" => {
      "lat" => coords[0],
      "long" => coords[1]
    }
  }
  if $locations.find({"name" => doc["name"]}).count == 0
    puts "mongoStore: " + doc.inspect
    $locations.insert(doc)
  end
end

def getCoordsFromRdf(stub)
  doc = Nokogiri::HTML(get("http://en.wikipedia.org/wiki/#{stub}"))
  out = doc.search(".geo").map {|i| i.text.split(";").map(&:strip).map(&:to_f) }.first
  puts "getCoords: " + out.inspect
  return out
end

